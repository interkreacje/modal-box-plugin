<?php
/**
 * Plugin Name: Modal box
 * Description: Wyskakujące okienko z informacją dla odwiedzających
 * Version: 1.0
 * Author: Miłosz from Aionline
 * Author URI: http://aionline.pl
*/

defined( 'WPINC' ) || die( 'Access restricted' );

class ModalBox
{
    public function __construct() {
        add_action( 'admin_enqueue_scripts', array($this,'load_resources_dashboard'));
        add_action( 'admin_menu', array($this,'wpdocs_register_my_custom_menu_page'));
        add_action( 'admin_init', array($this,'modal_box_register_settings'));
        add_action( 'admin_footer', array($this,'uploader_js'));
        add_action( 'wp_footer', array($this,'frontend_modalbox'));

        add_action('wp_enqueue_scripts', array($this,'frontend_resources'));
        // add_action( 'admin_init', array($this,'wpse_60168_var_dump_and_die'));
    }

    public function frontend_resources(){
        wp_enqueue_script('modalbox_cookie_js', plugin_dir_url( __FILE__ ) . 'js/cookie.js', null, 1.0, true);
        wp_enqueue_script('modalbox_footer_js', plugin_dir_url( __FILE__ ) . 'js/main.js', null, 1.0, true);
        wp_register_style( 'modalbox_style', plugin_dir_url( __FILE__ ) . '/style/style.css', false, '1.0.0' );
        wp_enqueue_style( 'modalbox_style' );
    }

    public function load_resources_dashboard() {
        wp_register_style( 'modalbox_style', plugin_dir_url( __FILE__ ) . '/style/style.css', false, '1.0.0' );
        wp_enqueue_style( 'modalbox_style' );

        wp_enqueue_script('modalbox_cookie_js', plugin_dir_url( __FILE__ ) . 'js/cookie.js', null, 1.0, true);
        wp_enqueue_script('modalbox_footer_js', plugin_dir_url( __FILE__ ) . 'js/main.js', null, 1.0, true);

        if( !(empty( $_GET['page'] ) || "modal-box" !== $_GET['page']) ) {
            wp_enqueue_media();
        }

    }

    public function modal_box_register_settings() {
       add_option( 'modal_box_title', 'Wpisz tytuł okienka...');
       register_setting( 'modal_box_options_group', 'modal_box_title', 'modal_box_callback' );

       add_option( 'modal_box_content', 'Treść wyskakującego okienka');
       register_setting( 'modal_box_options_group', 'modal_box_content', 'modal_box_callback' );

       add_option( 'bgimg', '');
       register_setting( 'modal_box_options_group', 'bgimg', 'modal_box_callback' );

       add_option( 'modal_box_switcher_box_val', 1);
       register_setting( 'modal_box_options_group', 'modal_box_switcher_box_val', 'modal_box_callback' );
    }

    public function uploader_js(){
        ?>

        <script>
            jQuery(document).ready(function($){

                var custom_uploader
                  , click_elem = jQuery('.wpse-228085-upload')
                  , target = jQuery('input[name="bgimg"]')

                click_elem.click(function(e) {
                    e.preventDefault();
                    //If the uploader object has already been created, reopen the dialog
                    if (custom_uploader) {
                        custom_uploader.open();
                        return;
                    }
                    //Extend the wp.media object
                    custom_uploader = wp.media.frames.file_frame = wp.media({
                        title: 'Wybierz zdjęcie w tle',
                        button: {
                            text: 'Wybierz zdjęcie w tle'
                        },
                        multiple: false
                    });
                    //When a file is selected, grab the URL and set it as the text field's value
                    custom_uploader.on('select', function() {
                        attachment = custom_uploader.state().get('selection').first().toJSON();
                        target.val(attachment.url);
                        jQuery("#preview").css('background-image', 'url(' + attachment.url + ')');
                        jQuery("#preview").fadeIn();
                    });
                    //Open the uploader dialog
                    custom_uploader.open();
                });
            });

            jQuery('#modal1-click').trigger('click');
            jQuery('#modal1-click').click();
        </script>

        <?php
    }



    public function wpdocs_register_my_custom_menu_page() {
        add_menu_page(
            __( 'Okienko informacyjne', 'textdomain' ),
            'Modal Box!',
            'manage_options',
            'modal-box',
            array($this,'modal_box_page'),
            plugins_url( 'modal-box/img/icon-popup.png' ),
            80
        );
    }

    public function modal_box_page() {
        ?>

        <!-- Create a header in the default WordPress 'wrap' container -->
        <div class="wrap">


            <h2>Ustawienia wyskakującego okienka</h2>
            <?php settings_errors(); ?>

            <?php
                if( isset( $_GET[ 'tab' ] ) ) {
                    $active_tab = $_GET[ 'tab' ];
                } else{
                    $active_tab = 'display_options';
                }
            ?>

            <h2 class="nav-tab-wrapper">
                <a href="?page=modal-box&tab=display_options" class="nav-tab <?php echo $active_tab == 'display_options' ? 'nav-tab-active' : ''; ?>">Ustawienia</a>
                <a href="?page=modal-box&tab=review_modal" class="nav-tab <?php echo $active_tab == 'review_modal' ? 'nav-tab-active' : ''; ?>">Podgląd</a>
            </h2>



                <?php if($active_tab == 'display_options'): ?>
                    <form method="post" action="options.php" id="modal_form">
                        <?php settings_fields( 'modal_box_options_group' ); ?>


                        <h3><strong><label for="modal_box_title">Zaznacz checkbox aby włączyć wyskakujące okienko na stronie</label> <input type="checkbox" id="modal_box_switcher_box_val" name="modal_box_switcher_box_val" value="1" <?php checked(1, (int)get_option('modal_box_switcher_box_val'), true) ?> /></strong></h3>



                        <h3><strong><label for="modal_box_title">Tytuł wyskakującego okienka:</label></strong></h3>
                        <input type="text" id="modal_box_title" name="modal_box_title" value="<?php echo get_option('modal_box_title'); ?>" />

                        <h3><strong><label for="modal_box_content">Treść wyskakującego okienka:</label></strong></h3>
                        <?php
                            $content = get_option('modal_box_content');
                            wp_editor( $content, 'modal_box_content', $settings = array('textarea_rows'=> '10') );
                        ?> <br><br>

                        <h3><strong><label for="modal_box_content">Zdjęcie w tle:</label></strong></h3>

                        <input type="hidden" name="bgimg" value="<?php echo get_option('bgimg'); ?>" />
                        <div id="preview" <?php if(get_option('bgimg')): ?> style="display: block; background-image: url(<?php echo get_option('bgimg'); ?>);" <?php endif; ?>></div>

                        <button class="button wpse-228085-upload">Wybierz plik</button>


                        <?php submit_button(); ?>
                    </form>
                <?php elseif($active_tab == 'review_modal'): ?>


                <div class="demo-btns">
                    <div class="info">
                        <div class="buttons">
                            <p>
                                <a href="" data-modal="#modal" id="modal1-click" class="modal__trigger">Kliknij i uruchom POPUP</a>
                            </p>
                        </div>
                    </div>
                </div>

                <!-- Modal -->
                <div id="modal" class="modalPP modal__bg " role="dialog" aria-hidden="true" >
                    <div class="modal__dialog">
                        <div class="modal__content" style="background-image: url(<?php echo get_option('bgimg'); ?>);">
                            <h1><?php echo get_option('modal_box_title'); ?></h1>
                            <article>
                                <?php echo get_option('modal_box_content'); ?>
                            </article>

                            <!-- modal close button -->
                            <a href="" class="modal__close demo-close">
                                <svg class="" viewBox="0 0 24 24"><path d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"/><path d="M0 0h24v24h-24z" fill="none"/></svg>
                            </a>

                        </div>
                    </div>
                </div>



                <?php endif; ?>





        </div><!-- /.wrap -->

        <?php
    }

    public function frontend_modalbox(){

        if(checked(1, (int)get_option('modal_box_switcher_box_val'), false)):

        ?>

        <div class="demo-btns">
            <div class="info">
                <div class="buttons">
                    <p>
                        <a href="" data-modal="#modal" id="modal1-click" class="modal__trigger">Kliknij i uruchom POPUP</a>
                    </p>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div id="modal" class="modalPP modal__bg " role="dialog" aria-hidden="true" >
            <div class="modal__dialog">
                <div class="modal__content" style="background-image: url(<?php echo get_option('bgimg'); ?>);">
                    <h1><?php echo get_option('modal_box_title'); ?></h1>
                    <article>
                        <?php echo get_option('modal_box_content'); ?>
                    </article>

                    <!-- modal close button -->
                    <a href="" class="modal__close demo-close">
                        <svg class="" viewBox="0 0 24 24"><path d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"/><path d="M0 0h24v24h-24z" fill="none"/></svg>
                    </a>

                </div>
            </div>
        </div>

        <?php

        endif;
    }

}


$popup = new ModalBox();







